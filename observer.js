// define observable prototype
var Observable = {
  listeners: [ /* { event: String, fn: Function } */ ],
  _addListener: function(event, fn) {
    this.listeners.push({ event: event, fn: fn });
  },
  _removeListener: function(event) {
    this.listeners = this.listeners.filter(function(listenerObj) {
      return listenerObj.event !== event; 
    });
  },
  on: function(event, fn) {
    this._addListener(event, fn);  
  },
  off: function(event) {
    this._removeListener(event);  
  },
  listenTo: function(target, event, fn) {
    target._addListener(event, fn);
  },
  trigger: function(event) {
    this.listeners.filter(function(listenerObj) {
      return listenerObj.event === event; 
    }).forEach(function(listenerObj) {
      listenerObj.fn(listenerObj.event); 
    });
  },
};


// new class Person
var Person = function(name) {
  this.name = name;
};

// inherit from Observable
Person.prototype = Observable;

// define getName method
Object.defineProperty(Person.prototype, 'getName', {
  writable: false,
  value: function() {
    return this.name;  
  }
});


var jon    = new Person('Jon Snow');
var tyrion = new Person('Tyrion Lannister');

jon.listenTo(tyrion, 'greeting', function(event) {
  console.log('Received a ' + event + ' from ' + tyrion.getName());
});

jon.on('bye', function(event) {
  console.log('Received a ' + event + ' from ' + jon.getName());
});

tyrion.trigger('greeting'); // "Received a greeting from..."
jon.trigger('bye'); // "Received a bye from..."

jon.off('bye'); // unbinds all 'bye' listeners
jon.trigger('bye'); // does not fire anything